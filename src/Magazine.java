public class Magazine {

    private String nameMag;
    private String address;

    public Smartphone[] getSmartphones() {
        return smartphones;
    }

    private Smartphone[] smartphones;

    public Magazine(String nameMag, String address, Smartphone[] smartphones) {
        this.nameMag = nameMag;
        this.address = address;
        this.smartphones = smartphones;
    }



    public String getNameMag() {
        return nameMag;
    }

    public void setNameMag(String nameMag) {
        this.nameMag = nameMag;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }



    public void setSmartphones(Smartphone[] smartphones) {
        this.smartphones = smartphones;
    }
}
