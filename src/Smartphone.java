public class Smartphone {
    public Smartphone(String model, String brand, int price, double display) {
        this.model = model;
        this.brand = brand;
        this.price = price;
        this.display = display;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public double getDisplay() {
        return display;
    }

    public void setDisplay(int display) {
        this.display = display;
    }

    public String getSmartphones() {

        return String.format(
                "%s,%s,%d,%f",
                getBrand(),getModel(),getPrice(),getDisplay()
        );
    }

    private String model;
    private String brand;
    private int price;
    private double display;
}
