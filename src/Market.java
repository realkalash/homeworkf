import java.util.Arrays;

public class Market {


    public static Magazine[] GenerateData() {

        Magazine[] magazine = new Magazine[3];

        Smartphone[] smartphones = new Smartphone[5];

        {
            Smartphone smartphone = new Smartphone("Galaxy S8", "Samsung", 18000, 5.7d);
            smartphones[0] = smartphone;
            magazine[0] = new Magazine("Цитрус", "Плехановская 7", smartphones);
            magazine[0].setSmartphones(smartphones);
        }

        {
            Smartphone smartphone = new Smartphone("Redmi note 5", "Xiaomi", 6500, 6d);
            smartphones[1] = smartphone;
            magazine[0] = new Magazine("Цитрус", "Плехановская 7", smartphones);
        }

        {
            Smartphone smartphone = new Smartphone("Redmi note 6", "Xiaomi", 8000, 6d);
            smartphones[2] = smartphone;
            magazine[0] = new Magazine("Цитрус", "Плехановская 7", smartphones);
        }

        {
            Smartphone smartphone = new Smartphone("P8 lite", "Huawei", 4399, 5d);
            smartphones[3] = smartphone;
            magazine[0] = new Magazine("Цитрус", "Плехановская 7", smartphones);
        }
        {
            Smartphone smartphone = new Smartphone("Galaxy S8", "Samsung", 18000, 5.7d);
            smartphones[0] = smartphone;
            magazine[1] = new Magazine("Алло", "Героев Сталинграда 161/1", smartphones);
        }

        {
            Smartphone smartphone = new Smartphone("Redmi note 5", "Xiaomi", 6999, 6d);
            smartphones[1] = smartphone;
            magazine[1] = new Magazine("Алло", "Героев Сталинграда 161/1", smartphones);
        }

        {
            Smartphone smartphone = new Smartphone("P8 lite", "Huawei", 4000, 5d);
            smartphones[3] = smartphone;
            magazine[1] = new Magazine("Алло", "Героев Сталинграда 161/1", smartphones);
        }
        {
            Smartphone smartphone = new Smartphone("Galaxy S8", "Samsung", 15500, 5.7d);
            smartphones[0] = smartphone;
            magazine[2] = new Magazine("Мобилочка", "Ньютона 5", smartphones);
        }

        {
            Smartphone smartphone = new Smartphone("Redmi note 5", "Xiaomi", 6570, 6d);
            smartphones[1] = smartphone;
            magazine[2] = new Magazine("Мобилочка", "Ньютона 5", smartphones);
        }


        return magazine;
    }

    public void findChiper(Magazine[] magazines) {

        int countPrice;

        for (int i = 0; i < magazines.length; i++) {

            Smartphone[] smarti = magazines[i].getSmartphones();
            int str = smarti[i].getPrice();


            System.out.println(str);
        }
    }
}
